(function () {
    'use strict';

    angular
        .module('taskman', [
            'ngRoute',
            'taskman.controllers',
            'taskman.services'
        ]);
})();
