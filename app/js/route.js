angular.module('taskman')

    .config(function ($routeProvider, $locationProvider) {

        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');

        $routeProvider
            .when('/', {
                templateUrl: 'app/partials/tasksTable.html',
                controller: 'TasksController'
            })
            .when('/task/:currTaskId', {
                templateUrl: 'app/partials/tasksUnit.html',
                controller: 'TasksController',
                resolve: {
                    currTaskId: function ($route) {
                        return $route.current.params.currTaskId;
                    }
                }
            })
            .otherwise({
                redirectTo: '/'
            });

    });