(function() {
  'use strict';

  angular
    .module('taskman.services', [
      'taskman.services.taskListDataService'
    ]);

})();