(function () {
    'use strict';

    angular
        .module('taskman.controllers.main', [])
        .controller('TasksController', taskmanController);

    function taskmanController($location, $routeParams, $route, taskListDataService) {
        var self = this;

        //copying service data to work with a copy
        self.tasks = angular.copy(taskListDataService.data);

        self.currTask = 0;

        self.viewTask = function (currentTask){
            $location.path('/task/' + currentTask);
        };

        self.currTask = $routeParams.currTaskId;

    }

})();