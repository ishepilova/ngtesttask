(function() {
  'use strict';

  angular
    .module('taskman.controllers', [
      'taskman.controllers.main',
      //'taskman.controllers.page',
      'taskman.controllers.route'
    ]);

})();