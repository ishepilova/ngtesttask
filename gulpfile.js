var gulp = require('gulp');
var args = require('yargs').argv;
var config = require('./gulp.config.js')();
var del = require('del');
var connect = require('gulp-connect');

var $ = require('gulp-load-plugins')({lazy: true});

/*
** Running JSHint and JSCS on JavaScript files
*/
gulp.task('jstest', function () {
    log('Running JSHint and JSCS');
    return gulp
        .src(config.allJs)
        .pipe($.if(args.verbose, $.print()))
        .pipe($.jscs())
        .pipe($.jshint())
        .pipe($.jshint.reporter('jshint-stylish', {verbose: true}))
        .pipe($.jshint.reporter('fail'));
});

/*
** Compiling Less to CSS
*/
gulp.task('less', ['clean-css'], function () {
    log('Compiling Less --> CSS');
    return gulp
        .src(config.less)
        .pipe($.plumber())
        .pipe($.less())
        .pipe($.autoprefixer())
        .pipe(gulp.dest(config.temp))
        .pipe(connect.reload());
});

/*
** Clean CSS in temp folder
*/
gulp.task('clean-css', function (done) {
    var files = config.temp + '**/*.css';
    clean(files, done);
});

/*
** Watching & Compiling LESS
*/

gulp.task('watch-less', function () {
    gulp.watch([config.less], ['less']);
});

/*
** Watch JS and HTML
*/

gulp.task('html', function () {
  gulp.src(config.html)
    .pipe(connect.reload());
});

gulp.task('watch-html', function () {
  gulp.watch([config.html], ['html']);
});

gulp.task('watch-js', function () {
  gulp.watch([config.js], ['js-inject']);
});

/*
** Connect to server
*/

gulp.task('connect', function () {
  connect.server({
    port: config.server.port,
    livereload: config.server.livereload
  });
});

gulp.task('default', ['connect', 'watch-html', 'watch-less', 'watch-js']);

/*
** Wire up Bower dependencies
*/

gulp.task('wiredep', function () {
    var options = config.getWiredepOptions;
    var wiredep = require('wiredep').stream;
    
    log('Wire up Bower dependencies and app js and inject them into Html');
    
    return gulp
        .src(config.index)
        .pipe(wiredep(options))
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest(config.clientFolder));
});

gulp.task('inject', ['wiredep', 'less'], function () {
    
    log('Compiling CSS, injecting it and calling Wiredep');
    
    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.css)))
        .pipe(gulp.dest(config.clientFolder));
});

gulp.task('js-inject', function () {
    
    log('Injecting JS');
    
    return gulp
        .src(config.index)
        .pipe($.inject(gulp.src(config.js)))
        .pipe(gulp.dest(config.clientFolder));
});


/*
** Helping functions
*/

function clean(path, done) {
    log('Cleaning ' + $.util.colors.blue(path));
    del(path, done);
}

function log(msg) {
    if (typeof (msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}